package com.citi.training.staff.service;

import java.util.List;

import com.citi.training.staff.dao.EmployeeRepo;
import com.citi.training.staff.model.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is the main business logic class for Employees.
 * another comment for jenkins
 * @author Frank
 * @see Employee
 */
@Component
public class EmployeeService {

    @Autowired
    private EmployeeRepo employeeRepo;

    /**
     * Finds all Employees in the current repository.
    */
    public List<Employee> findAll() {
        return employeeRepo.findAll();
    }

    public Employee save(Employee employee) {
        return employeeRepo.save(employee);
    }

    public Employee update(String id, Employee employee) {
        employee.setId(id);

        return employeeRepo.save(employee);
    }

    public void delete(String id) {
        employeeRepo.deleteById(id);
    }
}
